// import the modules
const fs = require('fs');
const csv = require('csv-parser');


//function to find matches won per team per year  and write the output in matchesWonPerTeamPerYear.json file
function matchesPerYearPerTeam() {

  //create a empty array to store the data
  const matchesWonPerYearPerTeam = {};

  const matchesData = [];

  // read matches data
  fs.createReadStream('src/data/matches.csv')
    .pipe(csv())
    .on('data', (match) => {
      matchesData.push(match);
    })
    .on('end', () => {

        // iterate through matchesData
        matchesData.forEach(match => {
            const year = match.season;
            const winner = match.winner;
            if (!matchesWonPerYearPerTeam[year]) {
              matchesWonPerYearPerTeam[year] = {};
            }
            if (matchesWonPerYearPerTeam[year]) {
              if (matchesWonPerYearPerTeam[year][winner]) {
                matchesWonPerYearPerTeam[year][winner]++;
              } else {
                matchesWonPerYearPerTeam[year][winner] = 1;
              }
            }
          });

      //Write the output to a JSON file
    fs.writeFile('src/public/output/matchesWonPerTeamPerYear.json', JSON.stringify(matchesWonPerYearPerTeam, null, 2), (err) => {
        console.log(`Output written to ${'src/public/output/matchesWonPerTeamPerYear.json'}`);
      });

    });

}

// call the function 
matchesPerYearPerTeam();