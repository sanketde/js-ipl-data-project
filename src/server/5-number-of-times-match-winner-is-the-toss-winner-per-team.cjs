const fs = require('fs');
const csv = require('csv-parser');


//function to find number of times a match winner is toss winner and write the output in a json file
function numberOfTimesTossWinnerIsMatchWinner() {
    const matchesData = [];
    const teamVsWinCount = {};

    // read matches data
    fs.createReadStream('src/data/matches.csv')
        .pipe(csv())
        .on('data', (match) => {
            matchesData.push(match);
        })
        .on('end', () => {

            // iterate through each match in matchesData
            matchesData.forEach(match => {
                let tossWinner = match['toss_winner'];
                let winner = match['winner'];

                if (tossWinner === winner) {
                    if (!teamVsWinCount[tossWinner]) {
                        teamVsWinCount[tossWinner] = 0;
                    }
                    teamVsWinCount[tossWinner]++;
                }

            });

            // //Write the output to a JSON file
            fs.writeFile('src/public/output/numberOfTimesMatchWinnerIsTheTossWinnerPerTeam.json', JSON.stringify(teamVsWinCount, null, 2), (err) => {
                console.log('Output written numberOfTimesMatchWinnerIsTheTossWinnerPerTeam.json');
            });

        });

}

numberOfTimesTossWinnerIsMatchWinner()
