const fs = require('fs');
const csv = require('csv-parser')




//function to find strike rate for each bats man in every season  and write the output in json file
function findStrikeRate() {
    const matchesData = [];
    const deliveriesData = [];
    const idVsSeason = {};
    const seasonVsBatsmanRuns = {};


    // read the matches data
    fs.createReadStream('src/data/matches.csv')
        .pipe(csv())
        .on('data', (match) => {
            matchesData.push(match);
        })
        .on('end', () => {

            // iterate through each match and create id vs season object
            matchesData.forEach(match => {
                let season = match['season'];
                let id = match['id'];
                idVsSeason[id] = season;
            });

            // read the deliveries data
            fs.createReadStream('src/data/deliveries.csv')
                .pipe(csv())
                .on('data', (delivery) => {
                    deliveriesData.push(delivery);
                })
                .on('end', () => {

                    // iterate through deliveries data
                    deliveriesData.forEach(delivery => {

                        let matchId = delivery['match_id'];
                        //get the season from the matchId
                        let season = idVsSeason[matchId];
                        let batsman = delivery['batsman'];
                        let batsmanRuns = parseInt(delivery['batsman_runs']);

                        if (!seasonVsBatsmanRuns[season]) {
                            seasonVsBatsmanRuns[season] = {};
                        }
                        if (!seasonVsBatsmanRuns[season][batsman]) {
                            seasonVsBatsmanRuns[season][batsman] = { runs: 0, balls: 0 };
                        }
                        seasonVsBatsmanRuns[season][batsman].runs += batsmanRuns;
                        seasonVsBatsmanRuns[season][batsman].balls++;
                    });

                    // iterate through each season and calculate the strike rate
                    for (season in seasonVsBatsmanRuns) {
                        for (batsman in seasonVsBatsmanRuns[season]) {
                            const runs = seasonVsBatsmanRuns[season][batsman].runs;
                            const balls = seasonVsBatsmanRuns[season][batsman].balls;
                            strikeRate = ((runs / balls) * 100).toFixed(2);
                            seasonVsBatsmanRuns[season][batsman] = { strike_rate: strikeRate };
                        }
                    }


                    //Write the output to a JSON file
                    fs.writeFile('src/public/output/strikerateOfBatsmanEachSeason.json', JSON.stringify(seasonVsBatsmanRuns, null, 2), (err) => {
                        console.log('Output written to strikerateOfBatsmanEachSeason.json file');
                    });
                })



        });
}

// call the function
findStrikeRate();