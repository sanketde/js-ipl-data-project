const fs = require('fs');
const csv = require('csv-parser');


//function to find the bowler with best economy in super overs and write output in a json file
function findBowlerWithBestEconomyInSuperOvers() {

}
const deliveriesData = [];
let economyRates = {};


fs.createReadStream('src/data/deliveries.csv')
    .pipe(csv())
    .on('data', (delivery) => {
        deliveriesData.push(delivery);
    })
    .on('end', () => {

        // iterate through each delivery in deliveriesDatas
        deliveriesData.forEach(delivery => {
            const isSuperOver = delivery['is_super_over'] == 1;
            if (isSuperOver) {
                const bowler = delivery['bowler'];
                const totalRuns = parseInt(delivery['total_runs']);
                const isWideBall = delivery['wide_runs'] !== '0';
                const isNoBall = delivery['noball_runs'] !== '0';

                //if the ball is not wide not no ball then consider that ball for count
                if (!isWideBall && !isNoBall) {
                    if (!economyRates[bowler]) {
                        economyRates[bowler] = { runs: 0, balls: 0 };
                    }
                    economyRates[bowler].runs += totalRuns;
                    economyRates[bowler].balls++;
                }
            }
        });

        // get the bowlers from ecomonyRates Object
        const bowlers = Object.keys(economyRates);

        // iterate the bowlers calculate economy of bowler
        bowlers.forEach((bowler) => {
            const runs = economyRates[bowler].runs;
            const balls = economyRates[bowler].balls;
            const economyRate = (runs / (balls / 6)).toFixed(2);
            economyRates[bowler] = {}
            economyRates[bowler].economyRate = economyRate;

        });

        // Convert the object to an array of key-value pairs
        const economyRatesArray = Object.entries(economyRates);

        // Sort the array based on the economy rate
        economyRatesArray.sort((bowler1, bowler2) => parseFloat(bowler1[1].economyRate) - parseFloat(bowler2[1].economyRate));

        //get the top economical bowler in super over
        let topBowler = economyRatesArray.slice(0, 1);
        topBowler = Object.fromEntries(topBowler);


        // //Write the output to a JSON file
        fs.writeFile('src/public/output/bowlerWithBestEconomyInSuperOvers.json', JSON.stringify(topBowler, null, 2), (err) => {
            console.log('Output written to bowlerWithBestEconomyInSuperOvers.json');
        });


    });

// call the function
findBowlerWithBestEconomyInSuperOvers()