const fs = require('fs');
const csv = require('csv-parser')




//function to find extra runs conceded per team in year 2016 and write the output in extraRunsConcededPerTeamIn2016.json file
function extraRunsConcededPerTeamIn2016() {
    const matchesData = [];
    const deliveriesData = [];
    const teamVsExtraRuns = {};

    // read matches data
    fs.createReadStream('src/data/matches.csv')
        .pipe(csv())
        .on('data', (match) => {
            matchesData.push(match);
        })
        .on('end', () => {
            let matchIdsof2016 = []
            matchesData.forEach(match => {
                if (match.season == 2016) {
                    matchIdsof2016.push(match.id)
                }
            });

            // read deliveries data
            fs.createReadStream('src/data/deliveries.csv')
                .pipe(csv())
                .on('data', (delivery) => {
                    deliveriesData.push(delivery);
                })
                .on('end', () => {

                    deliveriesData.forEach(delivery => {
                        if (matchIdsof2016.includes(delivery.match_id)) {

                            let team = delivery['bowling_team'];
                            let extraRuns = parseInt(delivery['extra_runs']);

                            if (!teamVsExtraRuns[team]) {
                                teamVsExtraRuns[team] = 0
                            }
                            teamVsExtraRuns[team] += extraRuns;
                        }
                    });

                    //Write the output to a JSON file
                    fs.writeFile('src/public/output/extraRunsConcededPerTeamIn2016.json', JSON.stringify(teamVsExtraRuns, null, 2), (err) => {
                        console.log('Output written to extraRunsConcededPerTeamIn2016.json file');
                    });
                })



        });

}

// call the function
extraRunsConcededPerTeamIn2016();