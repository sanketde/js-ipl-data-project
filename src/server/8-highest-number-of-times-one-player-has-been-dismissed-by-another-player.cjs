const fs = require('fs');
const csv = require('csv-parser')


//function to find highest number of dismissal and write output in a json file
function findNumberOfTimeOnePlayerDismissedByAnotherPlayer() {
    const deliveriesData = [];
    const playerVsDismissedBy = {};


    fs.createReadStream('src/data/deliveries.csv')
        .pipe(csv())
        .on('data', (delivery) => {
            deliveriesData.push(delivery);
        })
        .on('end', () => {
            // iterate through each delivery in deliveriesData
            deliveriesData.forEach(delivery => {

                let bowler = delivery['bowler'];
                let playerDismissed = delivery['player_dismissed'];


                if (delivery['player_dismissed'].length > 0) {
                    key = bowler + ", " + playerDismissed
                    // console.log(key)
                    if (!playerVsDismissedBy[key]) {
                        playerVsDismissedBy[key] = 0;
                    }
                    playerVsDismissedBy[key] += 1;
                }

            });

            const playerDismissedList = Object.entries(playerVsDismissedBy);
            playerDismissedList.sort((ele1, ele2) => ele2[1] - ele1[1]);
            let result = playerDismissedList[1]
            let players = result[0].split(',');
            const resultObj = {
                bowler: players[0],
                player_dismissed: players[1],
                times: result[1]
            }
            // //Write the output to a JSON file
            fs.writeFile('src/public/output/highestNumberOfTimesOnePlayerHasBeenDismissedByAnotherPlayer.json', JSON.stringify(resultObj, null, 2), (err) => {
                console.log('Output written to highestNumberOfTimesOnePlayerHasBeenDismissedByAnotherPlayer.json');
            });

        });

}


// call the function
findNumberOfTimeOnePlayerDismissedByAnotherPlayer()