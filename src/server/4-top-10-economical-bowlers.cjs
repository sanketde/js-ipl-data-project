const fs = require('fs');
const csv = require('csv-parser');


//function to find top 10 economical bowlers and write the output in top10EconomicalBowler.json file
function top10EconomicalBowler() {
    const matchesData = [];
    const deliveriesData = [];
    const matchIdsof2015 = [];
    let economyRates = {};

    // read matches data
    fs.createReadStream('src/data/matches.csv')
        .pipe(csv())
        .on('data', (match) => {
            matchesData.push(match);
        })
        .on('end', () => {
            // extract match_ids of 2015
            matchesData.forEach(match => {
                if (match.season == 2015) {
                    matchIdsof2015.push(match.id)
                }
            });

            // read deliveries data
            fs.createReadStream('src/data/deliveries.csv')
                .pipe(csv())
                .on('data', (delivery) => {
                    deliveriesData.push(delivery);
                })
                .on('end', () => {

                    // count runs and balls for each bowler
                    deliveriesData.forEach(delivery => {
                        if (matchIdsof2015.includes(delivery.match_id)) {
                            const bowler = delivery['bowler'];
                            const totalRuns = parseInt(delivery['total_runs']);
                            const isWideBall = delivery['wide_runs'] !== '0';
                            const isNoBall = delivery['noball_runs'] !== '0';

                            //if the ball is not wide not no ball then consider that ball for count
                            if (!isWideBall && !isNoBall) {
                                if (!economyRates[bowler]) {
                                    economyRates[bowler] = { runs: 0, balls: 0 };
                                }
                                economyRates[bowler].runs += totalRuns;
                                economyRates[bowler].balls++;
                            }

                        }
                    });

                    // get the bowlers from ecomonyRates Object
                    const bowlers = Object.keys(economyRates);

                    // iterate the bowlers calculate economy of bowler
                    bowlers.forEach((bowler) => {
                        const runs = economyRates[bowler].runs;
                        const balls = economyRates[bowler].balls;
                        const economyRate = (runs / (balls / 6)).toFixed(2);
                        economyRates[bowler] = {}
                        economyRates[bowler].economyRate = economyRate;

                    });

                    // Convert the object to an array of key-value pairs
                    const economyRatesArray = Object.entries(economyRates);

                    // Sort the array based on the economy rate
                    economyRatesArray.sort((bowler1, bowler2) => parseFloat(bowler1[1].economyRate) - parseFloat(bowler2[1].economyRate));


                    //get the top 10 economical bowlers
                    const top10bowlerArray = economyRatesArray.slice(0, 10);

                    const top10bowler = {}
                    // convert to object
                    for (const [bowler, economyRate] of top10bowlerArray) {
                        top10bowler[bowler] = economyRate;
                    }



                    // //Write the output to a JSON file
                    fs.writeFile('src/public/output/top10EconomicalBowler.json', JSON.stringify(top10bowler, null, 2), (err) => {
                        console.log('Output written to top10EconomicalBowler.json file');
                    });
                })



        });

}


// call the function
top10EconomicalBowler()