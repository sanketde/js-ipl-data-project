// import the modules
const fs = require('fs');
const csv = require('csv-parser');

//function to player won the highest number of player of the match each season  and write the output in json file

function findPlayerWonTheHighestNumberOfPlayerOfTheMatchEachSeason() {
    const matchesData = [];
    let seasonVsPlayerOfMatch = {};

    // read matches data
    fs.createReadStream('src/data/matches.csv')
        .pipe(csv())
        .on('data', (match) => {
            matchesData.push(match);
        })
        .on('end', () => {

            // iterate through each match in matches data
            matchesData.forEach(match => {
                const season = match['season'];
                const playerOfMatch = match['player_of_match'];

                if (!seasonVsPlayerOfMatch[season]) {
                    seasonVsPlayerOfMatch[season] = {}
                }
                if (!seasonVsPlayerOfMatch[season][playerOfMatch]) {
                    seasonVsPlayerOfMatch[season][playerOfMatch] = 0;
                }
                seasonVsPlayerOfMatch[season][playerOfMatch] += 1;

            });
            for (season in seasonVsPlayerOfMatch) {
                const playerList = Object.entries(seasonVsPlayerOfMatch[season]);
                playerList.sort((ele1, ele2) => -(ele1[1] - ele2[1]));
                let topPlayer = playerList.slice(0, 1);
                seasonVsPlayerOfMatch[season] = Object.fromEntries(topPlayer);
            }

            //Write the output to a JSON file
            fs.writeFile('src/public/output/playerWonTheHighestNumberOfPlayerOfTheMatchEachSeason.json', JSON.stringify(seasonVsPlayerOfMatch, null, 2), (err) => {
                console.log(`Output written to ${'src/public/output/playerWonTheHighestNumberOfPlayerOfTheMatchEachSeason.json'}`);
            });

        });

}


// call the function for testing
findPlayerWonTheHighestNumberOfPlayerOfTheMatchEachSeason()






