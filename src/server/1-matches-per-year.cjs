// import the modules
const fs = require('fs');
const csv = require('csv-parser');

//function to find matches per year in the ipl  and write the output in matchesPerYear.json file
function matchesPerYear() {

  const matchesPerYear = {};
  const matchesData = [];

  // read the matches data
  fs.createReadStream('src/data/matches.csv')
    .pipe(csv())
    .on('data', (match) => {
      matchesData.push(match);
    })
    .on('end', () => {

      // iterate through each match in matchesData
      matchesData.forEach(match => {
        const season = match['season'];
        if (matchesPerYear[season]) {
          matchesPerYear[season]++;
        } else {
          matchesPerYear[season] = 1;
        }
      });

      //Write the output to a JSON file
      fs.writeFile('src/public/output/matchesPerYear.json', JSON.stringify(matchesPerYear, null, 2), (err) => {
        console.log(`Output written to ${'src/public/output/matchesPerYear.json'}`);
      });

    });

}

// call the function for testing
matchesPerYear()






